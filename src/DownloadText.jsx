import React from 'react';
// TODO
const DownloadText = ({ getName, getText }) => {
  function textDownload(filename, text) {
    const element = document.createElement('a');
    element.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(text)}`);
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  const downloadHandler = () => {
    textDownload(`${getName}.txt`, getText);
  };

  return (
    <div>
      <button type="button" onClick={downloadHandler}>
        Download text
      </button>
    </div>
  );
};

export default DownloadText;
