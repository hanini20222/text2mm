import React from 'react';
import * as d3 from 'd3';
import { createMindMap } from './mindMapD3';
// TODO
const DownloadSvg = ({ getName, getData }) => {
  const css = `.node text {
    font-size: 13px;
  }

  .node--internal circle {
    fill: #4c586f;
  }

  .node--internal text {
    text-shadow: 1px 1px 0px #ffffff, 1px -1px 0px #ffffff, -1px -1px 0px #ffffff, -1px 1px 0px #ffffff;
  }

  .link {
    fill: none;
    stroke: #a2aab0;
    stroke-opacity: 0.3;
    stroke-width: 1px;
  }`;

  function svgDownload(filename, data) {
    const svg = d3.select('body').append('svg');
    svg.append('style').text(css);
    svg.append(() => createMindMap(data, null));
    svg.attr('viewBox', function autoBox() {
      const { x, y, width, height } = this.getBBox();
      return [x, y, width, height];
    });

    // svg.style('font', 'Helvetica');
    console.log(svg.node());
    const serializer = new XMLSerializer();
    const source = serializer.serializeToString(svg.node());
    svg.remove();

    const element = document.createElement('a');
    element.setAttribute('href', `data:image/svg+xml;charset=utf-8,${encodeURIComponent(source)}`);
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  const downloadHandler = () => {
    svgDownload(`${getName}.svg`, getData);
  };

  return (
    <div>
      <button type="button" onClick={downloadHandler}>
        Download SVG (without tooltip)
      </button>
    </div>
  );
};

export default DownloadSvg;
