import marked from 'marked';

marked.setOptions({
  pedantic: false,
  gfm: true,
  breaks: false,
  headerIds: false
});

const sanitarize = text => {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '/': '&#x2F;'
  };
  const reg = /[&<>"'/]/gi;
  return text.replace(reg, match => map[match]);
};

const numberOfTabs = text => {
  let count = 1;
  let index = 0;
  while (text.charAt(index) === '\t') {
    count += 1;
    index += 1;
  }
  return count;
};

const node = (name, level) => {
  const children = [];
  let parent = null;
  const note = '';
  return {
    name,
    note,
    children,
    level: () => (level === undefined ? -1 : level),
    parent: () => parent, // as a function to prevent circular reference when parse to JSON
    setParent: p => {
      parent = p;
    },
    setNote(n) {
      this.note = n;
    },
    appendChildren(c) {
      children.push(c);
      c.setParent(this);
      return this;
    }
  };
};

/*
  First === trigers note recording.
  Second === trigers note response 
  Expected format
  ...
  Ignored text
  ===
  Note1
  Note2
  ===
  Ignored text
  ...
  */

const note = () => {
  let started = false;
  let noteOutput = '';
  return name => {
    if (name === '===') {
      if (started) {
        started = false;
        return noteOutput;
      }
      started = true;
      noteOutput = '';
      return '===';
    }
    if (started) {
      noteOutput += `${name}\n`;
      return '===';
    }
    return '';
  };
};

const textToObject = (text, rootName) => {
  const getNote = note();

  const appendRec = (prev, curr) => {
    let currNode;
    if (typeof curr === 'string') {
      // In the recursive call it's a object
      const name = curr.trim();
      const level = numberOfTabs(curr);
      const fullNote = getNote(name);
      // console.log(prev.test());
      // Skip. Empty line or receiving Note
      if (name === '' || fullNote === '===') {
        return prev;
      }
      // Skip. Note has been received. Update previos object
      if (fullNote !== '') {
        prev.setNote(marked(fullNote));
        return prev;
      }
      // Create node object
      currNode = node(name, level);
    } else {
      currNode = curr;
    }
    if (currNode.level() > prev.level()) {
      // curr is prev's child
      prev.appendChildren(currNode);
    } else if (currNode.level() < prev.level()) {
      appendRec(prev.parent(), currNode); // recursive call to find the right parent level
    } else {
      // curr is prev's sibling
      prev.parent().appendChildren(currNode);
    }

    return currNode;
  };

  const root = node(sanitarize(rootName));

  sanitarize(text)
    .split('\n')
    .reduce(appendRec, root);

  return JSON.parse(JSON.stringify(root));
};

export default textToObject;
