/* eslint-disable react/prop-types */
import React, { useRef, useEffect } from 'react';
import { mindMapD3 } from './mindMapD3';

const MindMap = ({ data }) => {
  const svgRef = useRef(null);
  const tooltipRef = useRef(null);

  useEffect(() => {
    if (data && svgRef.current && tooltipRef.current) {
      mindMapD3(data, svgRef.current, tooltipRef.current);
    }
  }, [data, svgRef, tooltipRef]);

  return (
    <>
      <div ref={tooltipRef} />
      <svg ref={svgRef} />
    </>
  );
};

export default MindMap;
