import * as d3 from 'd3';

const removeAllG = svgRef => {
  // Remove old g
  d3.select(svgRef)
    .select('g')
    .remove();
};

const rootData = data => {
  const size = 800;
  const radius = size / 2;
  return d3
    .tree()
    .size([2 * Math.PI, radius])
    .separation((a, b) => (a.parent === b.parent ? 1 : 6) / a.depth)(d3.hierarchy(data));
};

const createMindMap = (data, div, divClass = 'tooltip') => {
  // Prepare data
  const root = rootData(data);
  // Create g and links
  const g = d3.create('svg:g');
  g.append('g')
    .selectAll('.link')
    .data(root.links())
    .enter()
    .append('path')
    .attr('class', 'link')
    .attr(
      'd',
      d3
        .linkRadial()
        .angle(d => d.x)
        .radius(d => d.y)
    );

  // Node
  const node = g
    .append('g')
    .selectAll('.node')
    .data(root.descendants())
    .enter()
    .append('g')
    .attr('class', d => {
      return `node${d.children ? ' node--internal' : ' node--leaf'}`;
    })
    .attr(
      'transform',
      d => `
          rotate(${(d.x * 180) / Math.PI - 90})
          translate(${d.y},0)
        `
    );
  // Text

  node
    .append('text')
    .attr('dy', '0.31em')
    .attr('x', d => {
      const pos = d.x < Math.PI;
      return pos === !d.children ? 5 : -5;
    })
    .attr('text-anchor', d => {
      const pos = d.x < Math.PI;
      return pos === !d.children ? 'start' : 'end';
    })
    .attr('transform', d => (d.x >= Math.PI ? 'rotate(180)' : null))
    .text(d => d.data.name);
  // Div given
  if (div) {
    div.attr('class', divClass).style('display', 'none');
    node
      .style('cursor', d => (d.data.note ? 'pointer' : 'move'))
      .style('text-decoration', d => (d.data.note ? 'underline' : 'none'))
      .on('mouseover', d => {
        if (d.data.note) {
          div.style('display', 'block').html(`<h1>${d.data.name}</h1>${d.data.note}`);
        }
      })
      .on('mouseout', () => {
        div.style('display', 'none').html('');
      });
  }
  return g.node();
};

//---------------

const mindMapD3 = (data, svgRef, tooltipRef) => {
  if (data && svgRef) {
    const div = d3.select(tooltipRef);
    // Remove old
    removeAllG(svgRef);
    // Draw new
    const svg = d3
      .select(svgRef)
      .call(
        d3.zoom().on('zoom', () => {
          svg.attr('transform', d3.event.transform);
        })
      )
      .append(() => createMindMap(data, div));

    d3.select(svgRef).attr('viewBox', function autoBox() {
      const { x, y, width, height } = this.getBBox();
      return [x, y, width, height];
    });
  }
};

// const downloadMindMapSvg = data => {
//   const svg = d3.create('svg');
//   svg.attr('width', '900px');
//   svg.attr('height', '900px');
//   const g = svg.append(() => createMindMap(data, null));
//   const bbox = svg.node();

//   svg.attr('viewBox', function autoBox() {
//     const { x, y, width, height } = this.getBBox();
//     return [x, y, width, height];
//   });
//   console.log(bbox);
//   return svg.node();
// };

export { mindMapD3, createMindMap };
