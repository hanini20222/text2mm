/* eslint-disable react/prop-types */
import React from 'react';
import DownloadText from './DownloadText';
import DownloadSvg from './DownloadSvg';
// TODO
const MindMapButtons = ({ getName, getText, getData }) => {
  return (
    <>
      <DownloadText getName={getName} getText={getText} />
      <DownloadSvg getName={getName} getData={getData} />
    </>
  );
};

export default MindMapButtons;
