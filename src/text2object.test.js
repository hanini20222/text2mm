import textToObject from './text2object';

describe('Function textToObject tests', () => {
  test('1 level. Single word', () => {
    const text = 'First';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: []
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Single word with xss characters', () => {
    const text = `First&<>"'/`;
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First&amp;&lt;&gt;&quot;&#x27;&#x2F;',
          note: '',
          children: []
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Single word, new line end', () => {
    const text = 'First\n\n\n';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: []
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Single word, new lines nefore and after', () => {
    const text = '\n\nFirstNewLine\n\n';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [{ name: 'FirstNewLine', note: '', children: [] }]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Item with spaces', () => {
    const text = 'First with space';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [{ name: 'First with space', note: '', children: [] }]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Item with tabs', () => {
    const text = 'First with space';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [{ name: 'First with space', note: '', children: [] }]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('1 level. Multiple 1 level siblings.', () => {
    const text = 'First\nFirst2';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [{ name: 'First', note: '', children: [] }, { name: 'First2', note: '', children: [] }]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('2 level. One 2 level item', () => {
    const text = 'First\n\tSecond';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('2 level. One 2 level item, three tabs', () => {
    const text = 'First\n\t\t\tSecond';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('2 level. Three 2 level items', () => {
    const text = 'First\n\tSecond1\n\tSecond2\n\tSecond3';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second1',
              note: '',
              children: []
            },
            {
              name: 'Second2',
              note: '',
              children: []
            },
            {
              name: 'Second3',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('3 level. Three 2 level items and two 3 level items. Name with tabs', () => {
    const text = 'First\n\tSecond1\tSecond2\n\t\tThird1\n\t\tThird2\n\tSecond2\n\tSecond3';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second1\tSecond2',
              note: '',
              children: [
                {
                  name: 'Third1',
                  note: '',
                  children: []
                },
                {
                  name: 'Third2',
                  note: '',
                  children: []
                }
              ]
            },
            {
              name: 'Second2',
              note: '',
              children: []
            },
            {
              name: 'Second3',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('3 level. Three 2 level items and two 3 level items, extra 1 level item', () => {
    const text = 'First\nFirst2\n\tSecond1\n\t\tThird1\n\t\tThird2\n\tSecond2\n\tSecond3';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: []
        },
        {
          name: 'First2',
          note: '',
          children: [
            {
              name: 'Second1',
              note: '',
              children: [
                {
                  name: 'Third1',
                  note: '',
                  children: []
                },
                {
                  name: 'Third2',
                  note: '',
                  children: []
                }
              ]
            },
            {
              name: 'Second2',
              note: '',
              children: []
            },
            {
              name: 'Second3',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('4 level. Three 2 level items and two 3 level items one 4 level item', () => {
    const text = 'First\n\tSecond1\n\t\tThird1\n\t\tThird2\n\t\t\tFour1\n\tSecond2\n\tSecond3';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second1',
              note: '',
              children: [
                {
                  name: 'Third1',
                  note: '',
                  children: []
                },
                {
                  name: 'Third2',
                  note: '',
                  children: [
                    {
                      name: 'Four1',
                      note: '',
                      children: []
                    }
                  ]
                }
              ]
            },
            {
              name: 'Second2',
              note: '',
              children: []
            },
            {
              name: 'Second3',
              note: '',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('Note test. 1 level single word and note', () => {
    const text = 'First\n===\nNote\nNote2\nNote3\n===\n\tSecond\n===\nLABAS\n===';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '<p>Note\nNote2\nNote3</p>\n',
          children: [
            {
              name: 'Second',
              note: '<p>LABAS</p>\n',
              children: []
            }
          ]
        }
      ]
    };

    expect(actual).toEqual(expectation);
  });
  test('Note test. 1 level single word and multi line notes', () => {
    const text = 'First\n\tSecond\n===\nNote\nNote2\nNote3\n===';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: [
            {
              name: 'Second',
              note: '<p>Note\nNote2\nNote3</p>\n',
              children: []
            }
          ]
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('Note test. 1 level single word unfinished note', () => {
    const text = 'First\n===\nNote\n';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: []
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
  test('Note test. 1 level just started note', () => {
    const text = 'First\n===\nNote';
    const rootName = 'root';
    const actual = textToObject(text, rootName);
    const expectation = {
      name: 'root',
      note: '',
      children: [
        {
          name: 'First',
          note: '',
          children: []
        }
      ]
    };
    expect(actual).toEqual(expectation);
  });
  //
});
