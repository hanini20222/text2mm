import React, { useState, useEffect } from 'react';

import MindMap from './MindMap';
import MindMapButtons from './MindMapButtons';

import text2object from './text2object';

import './sass/main.scss';

const App = () => {
  const initialName = 'Text 2 MindMap';
  const initialText = `Why ?
	Create MindMaps blazing fast
	===
	Traditional MindMap  tools is created as drawing tools.
	Takes lot of time to create larger MindMap
	===
	No learning curve, trivial as Notepad
	Learn concepts faster
	===
	You can add Notes for each element.

	Describe concept in details
	===
	Anki card supported
	===
	Export your MindMap  to Anki cards and repeat learning
 	 ===
	Markdown supported

What ?
	Structured information and  your thoughts
	Enhance your learning
	===
	The spatial layout helps you gain a better overview and makes new connections more visible so you can create an infinite number of thoughts, ideas, links and associations on any topic.
	===
	Plan and Organize
	===
	Evidence shows that Mind Mapping can be used to help you plan and organise your thinking before you start writing or get stuck into a project. 

You can develop all your ideas and see where they relate to each other before deciding the best way to go about things.
	===
	Problem Solving
	===
	A Mind Map can help you think with greater clarity to explore relationships between ideas and elements of an argument and to generate solutions to problems. It puts a new perspective on things by allowing you to see all the relevant issues and analyse choices in light of the big picture. It also makes it easier to integrate new knowledge and organise information logically as you aren’t tied to a rigid structure.
  ===
  


How ?
	Turn tab-indented lists into Mind Map
	Press Tab to indent lines
	Add notes describing in more details
	Place your note between  === 
	Notes support markdown
	===
	Write your rich formatted notes 
	# Heading 1
	## Heading 2
	### Heading 3
	### Heading 4
	### Heading 5
	### Heading 6
	_italic_
	**bold**
	**_bold and  italic_**
Add links
	[Contact developer](raibis.lt)
Add images
	![Mindmap](https://cdn.pixabay.com/photo/2016/06/20/19/55/mindmap-1469592__340.png)
>Block quotes

* List item
* List item
	===
	Zoom in and out using scroll
	You can drag Mindmap as well
	Export to Anki cards (Todo)
	Export svg (Todo)
	`;
  const [getName, setName] = useState(initialName);
  const [getText, setText] = useState(initialText);
  const [getData, setData] = useState(null);

  const nameChangetHandler = e => {
    setName(e.target.value);
  };

  useEffect(() => {
    setData(text2object(getText, getName));
  }, [getText, getName]);

  const textChangeHandler = e => {
    setText(e.target.value);
  };

  const keyDownHandler = e => {
    if (e.key === 'Tab') {
      e.preventDefault();
      const { selectionStart, selectionEnd } = e.target;
      e.target.value = `${getText.substring(0, selectionStart)}\t${getText.substring(selectionEnd)}`;
      e.target.selectionStart = selectionStart + 1;
      e.target.selectionEnd = selectionEnd + 1;
      textChangeHandler(e);
    }
  };

  return (
    <>
      <header className="mmm__header">
        <h1>Text 2 MindMap</h1>
      </header>
      <main className="mmm__main">
        <div className="mmm__text">
          <input type="text" placeholder="MindMap name" onChange={nameChangetHandler} value={getName} />
          <textarea value={getText} onKeyDown={keyDownHandler} onChange={textChangeHandler} placeholder="MindMap text" />
        </div>
        <div className="mmm__mindmap">
          <div className="mmm__mindmap--svg">
            <MindMap data={getData} />
          </div>
          <div className="mmm__mindmap--buttons">
            <MindMapButtons getName={getName} getText={getText} getData={getData} />
          </div>
        </div>
      </main>
    </>
  );
};

export default App;
